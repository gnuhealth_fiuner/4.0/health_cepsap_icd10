Módulo GNU Health para integrar CEPS-AP y CIE-10

Prerequisites
-------------

 * Python 3.6 or later (http://www.python.org/)
 * Tryton 5.0 (http://www.tryton.org/)

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

  Silix
  --------------
  website: http://www.silix.com.ar
  email: contacto@silix.com.ar

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
